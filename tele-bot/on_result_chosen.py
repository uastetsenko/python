from bot import *

def on_result(bot, update):
	query = update.inline_query.query
	logging.info(query)
	results = list()

	if query == "Неверный ИМЕЙ":
		text = onec.c42
		end_text = "👌 Мы нашли для вас инструкцию. Нажмите, что бы увидеть ответ"
		results.append(
			InlineQueryResultArticle(
				id=query.upper(),
				title=end_text,
				input_message_content=InputTextMessageContent(text, parse_mode='Markdown')
			)
		)
		bot.answerInlineQuery(update.inline_query.id, results)
	elif query == "Как вернуть товар" or query == "Возврат одной позиции":
		text = onec.c44
		end_text = "👌 Мы нашли для вас инструкцию. Нажмите, что бы увидеть ответ"
		results.append(
			InlineQueryResultArticle(
				id=query.upper(),
				title=end_text,
				input_message_content=InputTextMessageContent(text, parse_mode='Markdown')
			)
		)
		bot.answerInlineQuery(update.inline_query.id, results)
	elif query == "Ошибка пробития чека" or query == "Ошибка чека":
		text = onec.c45
		end_text = "👌 Мы нашли для вас инструкцию. Нажмите, что бы увидеть ответ"
		results.append(
			InlineQueryResultArticle(
				id=query.upper(),
				title=end_text,
				input_message_content=InputTextMessageContent(text, parse_mode='Markdown')
			)
		)
		bot.answerInlineQuery(update.inline_query.id, results)
	elif query == "У пользователя недостаточно прав" or query == 'Недостаточно прав':
		text = onec.c46
		end_text = "👌 Мы нашли для вас инструкцию. Нажмите, что бы увидеть ответ"
		results.append(
			InlineQueryResultArticle(
				id=query.upper(),
				title=end_text,
				input_message_content=InputTextMessageContent(text, parse_mode='Markdown')
			)
		)
		bot.answerInlineQuery(update.inline_query.id, results)
	elif query == "Нет принтера в 1С" or query == "Принтер":
		text = onec.c47
		end_text = "👌 Мы нашли для вас инструкцию. Нажмите, что бы увидеть ответ"
		results.append(
			InlineQueryResultArticle(
				id=query.upper(),
				title=end_text,
				input_message_content=InputTextMessageContent(text, parse_mode='Markdown')
			)
		)
		bot.answerInlineQuery(update.inline_query.id, results)
	elif query == "Поле объекта не обнаружено" or query == "Поле не обнаружено":
		text = onec.c48
		end_text = "👌 Мы нашли для вас инструкцию. Нажмите, что бы увидеть ответ"
		results.append(
			InlineQueryResultArticle(
				id=query.upper(),
				title=end_text,
				input_message_content=InputTextMessageContent(text, parse_mode='Markdown')
			)
		)
		bot.answerInlineQuery(update.inline_query.id, results)
	elif query == "Как узнать имеи на остатках" or query == "Имеи на остатках":
		text = onec.c4015
		end_text = "👌 Мы нашли для вас инструкцию. Нажмите, что бы увидеть ответ"
		results.append(
			InlineQueryResultArticle(
				id=query.upper(),
				title=end_text,
				input_message_content=InputTextMessageContent(text, parse_mode='Markdown')
			)
		)
		bot.answerInlineQuery(update.inline_query.id, results)
	elif query == "Нельзя получить ИМЕИ до того как они будут отправлены" or query == "Имеи не отправлены":
		text = onec.c491
		end_text = "👌 Мы нашли для вас инструкцию. Нажмите, что бы увидеть ответ"
		results.append(
			InlineQueryResultArticle(
				id=query.upper(),
				title=end_text,
				input_message_content=InputTextMessageContent(text, parse_mode='Markdown')
			)
		)
		bot.answerInlineQuery(update.inline_query.id, results)
	elif query == "Задублировался ордер":
		text = onec.c492
		end_text = "👌 Мы нашли для вас инструкцию. Нажмите, что бы увидеть ответ"
		results.append(
			InlineQueryResultArticle(
				id=query.upper(),
				title=end_text,
				input_message_content=InputTextMessageContent(text, parse_mode='Markdown')
			)
		)
		bot.answerInlineQuery(update.inline_query.id, results)
	elif query == "Не подтягивается акция" or query == "Проблема с акцией":
		text = onec.c43
		end_text = "👌 Мы нашли для вас инструкцию. Нажмите, что бы увидеть ответ"
		results.append(
			InlineQueryResultArticle(
				id=query.upper(),
				title=end_text,
				input_message_content=InputTextMessageContent(text, parse_mode='Markdown')
			)
		)
		bot.answerInlineQuery(update.inline_query.id, results)
	elif query == "Как узнать проданный имей" or query == "Поданный имей":
		text = onec.c4012
		end_text = "👌 Мы нашли для вас инструкцию. Нажмите, что бы увидеть ответ"
		results.append(
			InlineQueryResultArticle(
				id=query.upper(),
				title=end_text,
				input_message_content=InputTextMessageContent(text, parse_mode='Markdown')
			)
		)
		bot.answerInlineQuery(update.inline_query.id, results)
	elif query == "Как узнать телефон клиента" or query == "Телефон клиента" or query == "Как узнать номер клиента":
		text = onec.c4013
		end_text = "👌 Мы нашли для вас инструкцию. Нажмите, что бы увидеть ответ"
		results.append(
			InlineQueryResultArticle(
				id=query.upper(),
				title=end_text,
				input_message_content=InputTextMessageContent(text, parse_mode='Markdown')
			)
		)
		bot.answerInlineQuery(update.inline_query.id, results)
	elif query == "Обрезаются ценники при печати" or query == "Обрезаются ценники":
		text = onec.c4014
		end_text = "👌 Мы нашли для вас инструкцию. Нажмите, что бы увидеть ответ"
		results.append(
			InlineQueryResultArticle(
				id=query.upper(),
				title=end_text,
				input_message_content=InputTextMessageContent(text, parse_mode='Markdown')
			)
		)
		bot.answerInlineQuery(update.inline_query.id, results)
	elif query == "Ошибка при подключения устройства":
		text = kass.kass33
		end_text = "👌 Мы нашли для вас инструкцию. Нажмите, что бы увидеть ответ"
		results.append(
			InlineQueryResultArticle(
				id=query.upper(),
				title=end_text,
				input_message_content=InputTextMessageContent(text, parse_mode='Markdown')
			)
		)
		bot.answerInlineQuery(update.inline_query.id, results)
	elif query == "Таймаут при чтении данных из сокета":
		text = kass.kass34
		end_text = "👌 Мы нашли для вас инструкцию. Нажмите, что бы увидеть ответ"
		results.append(
			InlineQueryResultArticle(
				id=query.upper(),
				title=end_text,
				input_message_content=InputTextMessageContent(text, parse_mode='Markdown')
			)
		)
		bot.answerInlineQuery(update.inline_query.id, results)
	elif query == "Как снять Z-отчет на следующий день" or query == "Как снять Z" or query == "Не сняли Z отчет":
		text = kass.kass35
		end_text = "👌 Мы нашли для вас инструкцию. Нажмите, что бы увидеть ответ"
		results.append(
			InlineQueryResultArticle(
				id=query.upper(),
				title=end_text,
				input_message_content=InputTextMessageContent(text, parse_mode='Markdown')
			)
		)
		bot.answerInlineQuery(update.inline_query.id, results)
	elif query == "Как изменить время на кассе" or query == "Время на кассе":
		text = kass.kass31
		end_text = "👌 Мы нашли для вас инструкцию. Нажмите, что бы увидеть ответ"
		results.append(
			InlineQueryResultArticle(
				id=query.upper(),
				title=end_text,
				input_message_content=InputTextMessageContent(text, parse_mode='Markdown')
			)
		)
		bot.answerInlineQuery(update.inline_query.id, results)
	elif query == "Как снять переодический отчет" or query == "Как снять переодический" or query == "Переодический":
		text = kass.kass32
		end_text = "👌 Мы нашли для вас инструкцию. Нажмите, что бы увидеть ответ"
		results.append(
			InlineQueryResultArticle(
				id=query.upper(),
				title=end_text,
				input_message_content=InputTextMessageContent(text, parse_mode='Markdown')
			)
		)
		bot.answerInlineQuery(update.inline_query.id, results)
	elif query == "График ИТ":
		text = common_ask.com21
		end_text = "👌 Мы нашли для вас инструкцию. Нажмите, что бы увидеть ответ"
		results.append(
			InlineQueryResultArticle(
				id=query.upper(),
				title=end_text,
				input_message_content=InputTextMessageContent(text, parse_mode='Markdown')
			)
		)
		bot.answerInlineQuery(update.inline_query.id, results)
	elif query == "Номера ИТ":
		text = common_ask.com22
		end_text = "👌 Мы нашли для вас инструкцию. Нажмите, что бы увидеть ответ"
		results.append(
			InlineQueryResultArticle(
				id=query.upper(),
				title=end_text,
				input_message_content=InputTextMessageContent(text, parse_mode='Markdown')
			)
		)
		bot.answerInlineQuery(update.inline_query.id, results)
	elif query == "Хэлпдеск" or query == "Help-Desk":
		text = common_ask.com23
		end_text = "👌 Мы нашли для вас инструкцию. Нажмите, что бы увидеть ответ"
		results.append(
			InlineQueryResultArticle(
				id=query.upper(),
				title=end_text,
				input_message_content=InputTextMessageContent(text, parse_mode='Markdown')
			)
		)
		bot.answerInlineQuery(update.inline_query.id, results)
	elif query == "PackageAsistant" or query == "Установщик ПО":
		text = common_ask.com24
		end_text = "👌 Мы нашли для вас инструкцию. Нажмите, что бы увидеть ответ"
		results.append(
			InlineQueryResultArticle(
				id=query.upper(),
				title=end_text,
				input_message_content=InputTextMessageContent(text, parse_mode='Markdown')
			)
		)
		bot.answerInlineQuery(update.inline_query.id, results)
	elif query == "Как добавить сотрудника":
		text = common_ask.com25
		end_text = "👌 Мы нашли для вас инструкцию. Нажмите, что бы увидеть ответ"
		results.append(
			InlineQueryResultArticle(
				id=query.upper(),
				title=end_text,
				input_message_content=InputTextMessageContent(text, parse_mode='Markdown')
			)
		)
		bot.answerInlineQuery(update.inline_query.id, results)
	elif query == "Как исправить перессорт" or query == "Перессорт":
		text = common_ask.com27
		end_text = "👌 Мы нашли для вас инструкцию. Нажмите, что бы увидеть ответ"
		results.append(
			InlineQueryResultArticle(
				id=query.upper(),
				title=end_text,
				input_message_content=InputTextMessageContent(text, parse_mode='Markdown')
			)
		)
		bot.answerInlineQuery(update.inline_query.id, results)
	elif query == "хелп" or query == "help" or query == "помоги" or query == "вопрос" or query == "список":
		desk ="Нажми на меня"
		results1 = InlineQueryResultArticle(
				id="1",
				title="❌ Неверный ИМЕЙ",
				description=desk,
				input_message_content=InputTextMessageContent(onec.c42, parse_mode='Markdown')
			)
		results2=InlineQueryResultArticle(
				id="2",
				title="🗳 Возврат одной позиции",
				description=desk,
				input_message_content=InputTextMessageContent(onec.c44, parse_mode='Markdown')
			)
		results3 = InlineQueryResultArticle(
				id="3",
				title="🚫 Ошибка пробития чека",
				description=desk,
				input_message_content=InputTextMessageContent(onec.c45, parse_mode='Markdown')
			)
		results4 = InlineQueryResultArticle(
				id="4",
				title="🚷 У пользователя недостаточно прав",
				description=desk,
				input_message_content=InputTextMessageContent(onec.c46, parse_mode='Markdown')
			)
		results5 = InlineQueryResultArticle(
			id="5",
			title="🙅‍♂ Поле объекта не обнаружено",
			description=desk,
			input_message_content=InputTextMessageContent(onec.c48, parse_mode='Markdown')
		)
		results6 = InlineQueryResultArticle(
			id="6",
			title="🤘 Как узнать имеи на остатках",
			description=desk,
			input_message_content=InputTextMessageContent(onec.c4015, parse_mode='Markdown')
		)
		results7 = InlineQueryResultArticle(
			id="7",
			title="🚚 Нельзя получить ИМЕИ до того как они будут отправлены",
			description=desk,
			input_message_content=InputTextMessageContent(onec.c491, parse_mode='Markdown')
		)
		results8 = InlineQueryResultArticle(
			id="8",
			title="🙈 Задублировался ордер",
			description=desk,
			input_message_content=InputTextMessageContent(onec.c492, parse_mode='Markdown')
		)
		results9 = InlineQueryResultArticle(
			id="9",
			title="💰 Не подтягивается акция",
			description=desk,
			input_message_content=InputTextMessageContent(onec.c43, parse_mode='Markdown')
		)
		results10 = InlineQueryResultArticle(
			id="10",
			title="💵 Как узнать проданный имей",
			description=desk,
			input_message_content=InputTextMessageContent(onec.c4012, parse_mode='Markdown')
		)
		results11 = InlineQueryResultArticle(
			id="11",
			title="☎ Как узнать телефон клиента",
			description=desk,
			input_message_content=InputTextMessageContent(onec.c4013, parse_mode='Markdown')
		)
		results12 = InlineQueryResultArticle(
			id="12",
			title="✂ Обрезаются ценники при печати",
			description=desk,
			input_message_content=InputTextMessageContent(onec.c4014, parse_mode='Markdown')
		)
		results13 = InlineQueryResultArticle(
			id="13",
			title="❌ Ошибка при подключения устройства",
			description=desk,
			input_message_content=InputTextMessageContent(kass.kass33, parse_mode='Markdown')
		)
		results14 = InlineQueryResultArticle(
			id="14",
			title="⭕ Таймаут при чтении данных из сокета",
			description=desk,
			input_message_content=InputTextMessageContent(kass.kass34, parse_mode='Markdown')
		)
		results15 = InlineQueryResultArticle(
			id="15",
			title="💣 Как снять Z-отчет на следующий день",
			description=desk,
			input_message_content=InputTextMessageContent(kass.kass35, parse_mode='Markdown')
		)
		results16 = InlineQueryResultArticle(
			id="16",
			title="🕓 Как изменить время на кассе",
			description=desk,
			input_message_content=InputTextMessageContent(kass.kass31, parse_mode='Markdown')
		)
		results17 = InlineQueryResultArticle(
			id="17",
			title="📈 Как снять переодический отчет",
			description=desk,
			input_message_content=InputTextMessageContent(kass.kass32, parse_mode='Markdown')
		)
		results18 = InlineQueryResultArticle(
			id="18",
			title="🗓 График ИТ",
			description=desk,
			input_message_content=InputTextMessageContent(common_ask.com21, parse_mode='Markdown')
		)
		results19 = InlineQueryResultArticle(
			id="19",
			title="📞 Номера ИТ",
			description=desk,
			input_message_content=InputTextMessageContent(common_ask.com22, parse_mode='Markdown')
		)
		results20 = InlineQueryResultArticle(
			id="20",
			title="🚨 Вопросы по Help Desk",
			description=desk,
			input_message_content=InputTextMessageContent(common_ask.com23, parse_mode='Markdown')
		)
		results21 = InlineQueryResultArticle(
			id="21",
			title="📲 Установщик ПО",
			description=desk,
			input_message_content=InputTextMessageContent(common_ask.com24, parse_mode='Markdown')
		)
		results22 = InlineQueryResultArticle(
			id="22",
			title="👨 Кто добавляет персонал в 1С",
			description=desk,
			input_message_content=InputTextMessageContent(common_ask.com25, parse_mode='Markdown')
		)
		results23 = InlineQueryResultArticle(
			id="23",
			title="🛠 Кто исправляет перессорт",
			description=desk,
			input_message_content=InputTextMessageContent(common_ask.com27, parse_mode='Markdown')
		)
		res = [results1, results2, results3, results4, results5, results6, results7, results8, results9, results10,
			   results11, results12, results13, results14, results15, results16, results17, results18, results19,
			   results20, results21, results22, results23]
		bot.answerInlineQuery(update.inline_query.id, res)
	elif query == "":
		pass
	else:
		results.append(
			InlineQueryResultArticle(
				id=query.upper(),
				title="Привет, Либо ты слишком сложно написал, либо инструкции нет",
				input_message_content=InputTextMessageContent(query.upper())
			)
		)
		bot.answerInlineQuery(update.inline_query.id, results)