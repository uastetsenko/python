from bot import *
import time
import requests
import json
from db import db, get_or_create_user


def get_html(url):
    response = requests.get(url)
    return response.text

def sayhello_not_start(bot, update, user_data):
    bot.send_photo(chat_id=update.message.chat.id, photo='http://darkmedia.pp.ua/images/zzz/godfather1.jpg')

def sayhello_start(bot, update, user_data):
    msg = update.message.text
    cht_id = update.message.chat.id
    user_id = update.message.from_user.id

    if update.message.chat.type == 'supergroup':
        if "анекдот" in msg.lower():
            html = get_html('http://rzhunemogu.ru/RandJSON.aspx?CType=1')
            g_text = html.replace('{"content":"', '').replace('"}', '')
            bot.send_message(update.message.chat.id, text=g_text)
        elif "стих" in msg.lower() or "стишок" in msg.lower():
            html = get_html('http://rzhunemogu.ru/RandJSON.aspx?CType=3')
            g_text = html.replace('{"content":"', '').replace('"}', '')
            bot.send_message(update.message.chat.id, text=g_text)
        elif "интересн" in msg.lower() or "историю" in msg.lower() or "рассказ" in msg.lower():
            html = get_html('http://rzhunemogu.ru/RandJSON.aspx?CType=2')
            g_text = html.replace('{"content":"', '').replace('"}', '')
            bot.send_message(update.message.chat.id, text=g_text)
        elif "афоризм" in msg.lower():
            html = get_html('http://rzhunemogu.ru/RandJSON.aspx?CType=4')
            g_text = html.replace('{"content":"', '').replace('"}', '')
            bot.send_message(update.message.chat.id, text=g_text)
        elif "цитату" in msg.lower():
            html = get_html('http://rzhunemogu.ru/RandJSON.aspx?CType=5')
            g_text = html.replace('{"content":"', '').replace('"}', '')
            bot.send_message(update.message.chat.id, text=g_text)
        elif "тост" in msg.lower():
            html = get_html('http://rzhunemogu.ru/RandJSON.aspx?CType=6')
            g_text = html.replace('{"content":"', '').replace('"}', '')
            bot.send_message(update.message.chat.id, text=g_text)
        elif 'админ' in msg or 'на смене' in msg:
            text = "Админы:\n\n" \
                   "👻 @oufinx\n" \
                   "🤡 @kirukysh\n" \
                   "🤟 @therion13\n\n" \
                   "Можете смело писать им 😃"
            bot.send_message(update.message.chat.id, text,
                             reply_to_message_id=update.message.message_id, parse_mode=ParseMode.MARKDOWN)
        elif "команды" in msg.lower():
            g_text = "Вот что тебе доступно:\n\n" \
                     "- `Бот, расскажи анекдот`\n" \
                     "- `Бот, расскажи стих`\n" \
                     "- `Бот, расскажи историю`\n" \
                     "- `Бот, расскажи цитату`\n" \
                     "- `Бот, расскажи тост`\n" \
                     "- `Бот, кто админ?`"
            bot.send_message(update.message.chat.id, text=g_text, parse_mode=ParseMode.MARKDOWN)

    if update.message.chat.type == 'supergroup' and (user_id == 149944565 or user_id == 376701110 or user_id == 232216124):
        if "забань" in msg:
            to_time_raw = msg[msg.find('на '):].replace('на ', '')
            to_time_col = int(to_time_raw[:to_time_raw.find(' ')].replace(' ', ''))
            to_time_met = to_time_raw[to_time_raw.find(' '):].replace(' ', '')

            if to_time_met == 'минуту' or to_time_met == 'минуты' or to_time_met == 'минут':
                good_time = int(time.time()) + int(to_time_col) * 60
            elif to_time_met == 'час' or to_time_met == 'часа' or to_time_met == 'часов':
                good_time = int(time.time()) + int(to_time_col) * 3600
            elif to_time_met == 'день' or to_time_met == 'дня' or to_time_met == 'дней':
                good_time = int(time.time()) + int(to_time_col) * 3600 * 24
            elif to_time_met == 'неделю' or to_time_met == 'недели' or to_time_met == 'недель':
                good_time = int(time.time()) + int(to_time_col) * 3600 * 24 * 7

            get_ban_id = update.message.reply_to_message.from_user.id

            if (get_ban_id == 149944565 or get_ban_id == 376701110 or get_ban_id == 232216124):
                text = "Админушка, ты дурак? 🤭"
                bot.send_message(update.message.chat.id, text,
                                 reply_to_message_id=update.message.message_id)
            else:
                text = "Я забанил его на " + to_time_raw + "\nТеперь он не сможет писать в чат! 👻"
                bot.restrict_chat_member(cht_id, get_ban_id, until_date=good_time)
                bot.send_message(update.message.chat.id, text,
                                 reply_to_message_id=update.message.message_id)

        elif 'ты тут?' in msg:
            text = "Я здесь 😎\n\n" \
                   "Так как ты админ, тебе доступны следующие функции:\n" \
                   "👉 Бан пользователей, просто скажи - `Бот, забань на *время*`"
            bot.send_message(update.message.chat.id, text,
                             reply_to_message_id=update.message.message_id, parse_mode=ParseMode.MARKDOWN)

    else:
        pass
