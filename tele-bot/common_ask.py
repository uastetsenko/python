from bot import *

maintext = '👮 Общие вопросы'

listofcommon =[[InlineKeyboardButton("🗓 График работы ИТ", callback_data='21'),
               InlineKeyboardButton("📞 Телефоны ИТ", callback_data='22')],
               [InlineKeyboardButton("🚨 Проблемы с Help Desk", callback_data='23'),
               InlineKeyboardButton("📲 Проблемы с PackageAsistant", callback_data='24')],
               [InlineKeyboardButton("👨 Кто добавляет персонал в 1С", callback_data='25')],
               [InlineKeyboardButton("💿 Кто устанавливает MobileSop", callback_data='26')],
               [InlineKeyboardButton("🛠 Кто исправляет пересорты", callback_data='27')],
               [InlineKeyboardButton("🔧 Кто редактирует продажи в 1С", callback_data='28')],
               [InlineKeyboardButton("🎉 Наши чаты в телеграмме", callback_data='29')],
               [InlineKeyboardButton("🏠 Домой", callback_data='999')]]
reply_i_kb_main = InlineKeyboardMarkup(listofcommon)

com3_kb =[[InlineKeyboardButton("Учетная запись заблокирована", callback_data='31')],
        [InlineKeyboardButton("Не работает картридер", callback_data='32')],
        [InlineKeyboardButton("Завис Help Desk", callback_data='33')],
        [InlineKeyboardButton("⬅️ Вернуться назад", callback_data='300')]]
reply_com3_kb = InlineKeyboardMarkup(com3_kb)

com21 = '📆 График работы:\n\n' \
           '🌑 *ПН* ➖ 09:30 - 20:00\n' \
           '🌒 *ВТ*  ➖ 09:30 - 20:00\n' \
           '🌓 *СР*  ➖ 09:30 - 20:00\n' \
           '🌕 *ЧТ*  ➖ 09:30 - 20:00\n' \
           '🌖 *ПТ*  ➖ 09:30 - 20:00\n' \
           '🌗 *СБ*  ➖ 10:00 - 19:00\n' \
           '🌘 *ВС*  ➖ 10:00 - 18:00\n\n' \
           'Что делать если есть проблема а ИТ-Отдел не работает?\n' \
        'Ответ прост - писать письма на почту 👉 *Support@zzz.ua*'

com22 = 'Номера телефонов ИТ-Отдела:\n\n' \
       '*LIFE* 👉 +38 (093) 111 11 11\n' \
       '  *KS*  👉 +38 (067) 111 11 11'

com23 = '🔥 Все что связано с программой Help Desk - это забота Kyivstar! 🔥'

com231 = '❗ Если при запуске Help Desk у вас всплывает окно с надписью "Учетная запись заблокирована, обратитесь к администратору системы", это значит что Kyivstar заблокировал вашу учетную запись по каким либо причинам.\n\n' \
        '🙏 Обращайтесь пожалуйста в Kyivstar'

com232 = '❗ Если вы нажимаете на иконку картридера и ничего не происходит.\n\n' \
        '🙏 Обращайтесь пожалуйста в Kyivstar'

com233 = '❗ Если у вас зависла программа Help Desk, черный/синий экран\n\n' \
        '1️⃣  *Нажмите* в Help Desk клавиши: *CTRL*+*ALT*+*END*\n' \
        '2️⃣  В меню выберите *Sing Out* или *Log Off* или *Выход*\n' \
        '3️⃣  Подождите пока закроется Help Desk\n' \
        '4️⃣  Зайдите в Help Desk поновой'
com24 = '❗ По поводу всех вопросов касательно работы программы Package Asistant обращайтесь к региональных менеджерам по услугам.\n\n' \
        'Либо опишите свою проблему в чате-поддержки "Impuls2000/ZZZ" в которую вас должен пригласить Актинский Андрей'
com25 = '❗ Добавить/Перевести/Уволить продавца в 1С может сделать только РРС!'
com26 = '❗ MobileSop устанавливает Виталий Хитрюк'
com27 = '❗ Пересортами занимается Финансовый Отдел'
com28 = '❗ Корректировками занимается Финансовый Отдел'

com29 = '❗ Пприсоединяйтесь к нашим чатам:\n\n' \
        'Поднебесный чат 👉 https://t.me/blablachatnotcorrect\n' \
        'Инфо канал 👉 https://t.me/joinchat/AAAAAE023114KjW-4mDQg'
