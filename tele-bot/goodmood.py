import random
import string

from bot import *

maintext = 'Данный раздел включает в себя полезные инструменты которые могут пригодиться в жизни:'

main_goodmood =[[InlineKeyboardButton("🎲 Генератор паролей", callback_data='61')],
			[InlineKeyboardButton("🏠 Домой", callback_data='999')]]
reply_main_goodmood = InlineKeyboardMarkup(main_goodmood)

def RandomNumber(col, size=10, chars=string.ascii_letters + string.digits):
    code = []
    for a in range(col):
        code.append(''.join(random.choice(chars) for _ in range(size)))
    return code

gm61 = 'Сколько генерировать?'

gm610 = 'Ваш пароль: '
