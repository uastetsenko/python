from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, \
    RegexHandler, CallbackQueryHandler, ConversationHandler, messagequeue as mq, InlineQueryHandler, \
    ChosenInlineResultHandler
from telegram import ReplyKeyboardMarkup, InlineKeyboardButton, InlineKeyboardMarkup, ParseMode, ReplyKeyboardRemove, \
    InlineQueryResultArticle, InputTextMessageContent

import logging

import settings
import common_ask
import main_page
import kass
import onec
import inline_button_pressed
import goodmood
import on_result_chosen
from sayhello import sayhello_start, sayhello_not_start
from agent import *

from db import db, get_or_create_user, get_subscribed

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=logging.INFO,
                    filename='bot.log'
                    )

CHANNEL_NAME = '@tttinformationchat'
text_for_chat = ['Это тебе не мелочь по карманам тырить 👉 @zzzsup_bot',
                 'Налетай, торопись, покупай живопись 👉 @zzzsup_bot',
                 'Люк, я твой инструктор 👉 @zzzsup_bot',
                 'Жить как говорится хорошо. А хорошо жить еще лучше 👉 @zzzsup_bot',
                 'Будь проклят тот день, когда я сел за баранку этого пылесоса! 👉 @zzzsup_bot',
                 'Кушать подано! Садитесь жрать, пожалуйста 👉 @zzzsup_bot',
                 'Тебе бы в парикмахерской из пульверизатора прыскать 👉 @zzzsup_bot',
                 'Если человек идиот, то это надолго 👉 @zzzsup_bot',
                 'Лопух! Такого возьмём без шума и пыли 👉 @zzzsup_bot',
                 'Не бойся, Козлодоев, буду бить аккуратно, но сильно 👉 @zzzsup_bot',
                 'Кто билетов купит пачку, тот получит водокачку 👉 @zzzsup_bot',
                 'Есть установка весело встретить Новый год 👉 @zzzsup_bot',
                 'Мы должны воспитывать нашего зрителя, голыми ногами его не воспитаешь 👉 @zzzsup_bot',
                 'Ну, граждане алкоголики, хулиганы и тунеядцы! Кто хочет поработать? 👉 @zzzsup_bot',
                 'Работа стоит, а срок идёт 👉 @zzzsup_bot',
                 'Скоро на тебя оденут деревянный макинтош и  в твоём доме будет тихо играть музыка, но ты её не услышишь 👉 @zzzsup_bot',
                 'У вас несчастные случаи на стройке были? Будут 👉 @zzzsup_bot',
                 'Всё что нажито непосильным трудом... Три магнитофона, три кинокамеры заграничных, три портсигара отечественных, куртка замшевая… три куртки… 👉 @zzzsup_bot',
                 'Замуровали, демоны! Вот что крест животворящий делает! 👉 @zzzsup_bot',
                 'Не учите меня жить, лучше помогите материально! 👉 @zzzsup_bot',
                 'В столовой и в бане все равны 👉 @zzzsup_bot',
                 'Это тебе не картошку варить. Это у них шуры-муры 👉 @zzzsup_bot',
                 'Какая же гадость эта ваша заливная рыба! 👉 @zzzsup_bot',
                 'Что вы меня поливаете?!! Я же не клумба! 👉 @zzzsup_bot',
                 'Понимаете, каждый год 31 декабря мы с друзьями ходим в баню. Это у нас такая традиция 👉 @zzzsup_bot',
                 'Нашлись добрые люди... Подогрели, обобрали. То есть подобрали, обогрели... 👉 @zzzsup_bot',
                 'Лично я хожу на службу только потому, что она меня облагораживает 👉 @zzzsup_bot',
                 'У меня такая безупречная репутация, что меня уже давно пора скомпрометировать 👉 @zzzsup_bot',
                 'Поставьте Веру на место! И не трогайте больше руками! 👉 @zzzsup_bot',
                 'Чует моё сердце, что мы накануне грандиозного шухера. 👉 @zzzsup_bot',
                 'Возьми все, я себе еще нарисую 👉 @zzzsup_bot',
                 'Позвольте вас спросить как художник художника: Вы рисовать умеете? 👉 @zzzsup_bot',
                 'Кино в массы, деньги в кассы 👉 @zzzsup_bot',
                 'Не смеши людей. Балерина 👉 @zzzsup_bot']

def clear_phone(bot, update, user_data):
    if update.message.chat.type == 'private':
        bot.send_document(chat_id=update.message.chat.id, document=open('files/clear_phone.docx', 'rb'))
    else:
        text = 'Тебе это недоступно'
        bot.send_message(update.message.chat.id, text=text)

def get_log(bot, update, user_data):
    if update.message.chat.type == 'private' and update.message.chat.username == 'oufinx':
        bot.send_document(chat_id=update.message.chat.id, document=open('bot.log', 'rb'))
    else:
        text = 'Тебе это недоступно'
        bot.send_message(update.message.chat.id, text=text)

def com_start(bot, update, user_data):
    user = get_or_create_user(db, update._effective_user, update.message)
    try:
        logging.info('User: %s', user['username'].encode('utf-8'))
    except AttributeError:
        logging.info('User: %s', user['first_name'].encode('utf-8'))

    i_kb_start = [[InlineKeyboardButton("🤘 Запустить супер-клавиатуру 🤘", callback_data='999')]]
    reply_i_kb_start = InlineKeyboardMarkup(i_kb_start)
    update.message.reply_text('Привет {}'.format(user['first_name']), reply_markup=ReplyKeyboardRemove())
    text = 'Я тот самый бот, который научит тебя справляться с проблемами в твоей работе ✌'
    update.message.reply_text(text, reply_markup=reply_i_kb_start)


def talk_to_me(bot, update, user_data):
    user = get_or_create_user(db, update._effective_user, update.message)
    user_text = 'Ты написал: {}'.format(update.message.text)
    try:
        logging.info('User: %s, Chat id (%s), Message - %s', user['username'], update.message.chat.id,
                     update.message.text.encode('utf-8'))
    except AttributeError:
        logging.info('User: %s, Chat id (%s), Message - %s', user['first_name'], update.message.chat.id,
                     update.message.text.encode('utf-8'))
    update.message.reply_text(user_text)

def super_kb(bot, update, user_data):
    if update.message.chat.type == 'private':
        update.message.reply_text(main_page.maintext,
            reply_markup=main_page.reply_main_kb)
    else:
        update.message.reply_text(main_page.maintext,
            reply_markup=main_page.reply_main_kb1)


@mq.queuedmessage
def send_updates(bot, job):
        for user in get_subscribed(db):
            text = 'Последние обновления:\n\n' \
                   'Добавлен новый пункт в раздел 1С:\n' \
                   '🤘 Как узнать любой ИМЕЙ числящийся на ТТ'
            bot.sendMessage(chat_id=user['chat_id'], text=text)

@mq.queuedmessage
def send_to_chat(bot, job):
    bot.sendMessage(CHANNEL_NAME, text=goodmood.random.choice(text_for_chat))

def main():
    mybot = Updater(settings.API_KEY)

    mybot.bot._msg_queue = mq.MessageQueue()
    mybot.bot._is_messages_queued_default = True

    # mybot.job_queue.run_repeating(send_updates, interval=10)
    mybot.job_queue.run_repeating(send_to_chat, interval=goodmood.random.randint(3600*24,3600*48))

    dp = mybot.dispatcher

    inline_caps_handler = InlineQueryHandler(on_result_chosen.on_result)
    dp.add_handler(inline_caps_handler)

    dp.add_handler(CommandHandler('start', com_start, pass_user_data=True))
    dp.add_handler(CommandHandler('clearphone', clear_phone, pass_user_data=True))
    dp.add_handler(CommandHandler('getlog', get_log, pass_user_data=True))
    dp.add_handler(RegexHandler('^(Супер-клавиатура)$', super_kb, pass_user_data=True))
    dp.add_handler(RegexHandler('(Привет)', com_start, pass_user_data=True))
    dp.add_handler(RegexHandler('(бот)', sayhello_not_start, pass_user_data=True))
    dp.add_handler(RegexHandler('(Бот)', sayhello_start, pass_user_data=True))
    dp.add_handler(CallbackQueryHandler(inline_button_pressed.inline_button_pressed))
    logging.info("Bot was start")

    get_code_handle = ConversationHandler(
        entry_points=[CommandHandler('code', get_code_first, pass_user_data=True),
                      RegexHandler('^(На эту почту)$', get_code_message, pass_user_data=True),
                      RegexHandler('^(Ввести новую)$', get_code_sec, pass_user_data=True),
                      RegexHandler('^(Ввести почту для кода активации)$', get_code_sec, pass_user_data=True),
                      RegexHandler('^(Отмена миссии)$', com_start, pass_user_data=True)],
        states={
            "mail": [MessageHandler(Filters.text, get_code_sec, pass_user_data=True)],
            "stats": [MessageHandler(Filters.text, get_code_mail, pass_user_data=True)],
            "save": [MessageHandler(Filters.text, get_code_save, pass_user_data=True)],
            "message": [MessageHandler(Filters.text, get_code_message, pass_user_data=True)]
        },
        fallbacks=[]
    )
    dp.add_handler(get_code_handle)

    # dp.add_handler(MessageHandler(Filters.text, talk_to_me, pass_user_data=True))

    mybot.start_polling()
    mybot.idle()

if __name__ == '__main__':
    main()