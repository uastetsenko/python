from bot import *
import agent
import goodmood

def get_cod(col):
    text = goodmood.RandomNumber(col)
    text2 = 'Ваши пароли:\n\n'
    for a in text:
        text2 += a + '\n'

    return text2

def inline_button_pressed(bot, update):
    check = False
    query = update.callback_query

    i_kb_back = [[InlineKeyboardButton("⬅️ Вернуться назад", callback_data='999')]]
    reply_i_kb_back = InlineKeyboardMarkup(i_kb_back)

    try:
        data = int(query.data)
        logging.info('User go to menu - %s', data)
        if data == 11:
            text = main_page.maintext
            check = False
            bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                  message_id=query.message.message_id, reply_markup=common_ask.reply_i_kb_main)
        elif data == 12:
            text = kass.maintext
            check = False
            bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                  message_id=query.message.message_id, reply_markup=kass.reply_i_kb_kass)
        elif data == 13:
            text = onec.maintext
            check = False
            bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                  message_id=query.message.message_id, reply_markup=onec.reply_i_kb_ones1)
        elif data == 14:
            text = agent.maintext
            check = False
            bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                  message_id=query.message.message_id, reply_markup=agent.reply_main_agent)
        elif data == 15:
            text = goodmood.maintext
            check = False
            bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                  message_id=query.message.message_id, reply_markup=goodmood.reply_main_goodmood)
        elif data == 21:
            text = common_ask.com21
            check = True
        elif data == 22:
            text = common_ask.com22
            check = True
        elif data == 23:
            text = common_ask.com23
            check = False
            bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                  message_id=query.message.message_id, reply_markup=common_ask.reply_com3_kb)
        elif data == 24:
            text = common_ask.com24
            check = True
        elif data == 25:
            text = common_ask.com25
            check = True
        elif data == 26:
            text = common_ask.com26
            check = True
        elif data == 27:
            text = common_ask.com27
            check = True
        elif data == 28:
            text = common_ask.com28
            check = True
        elif data == 29:
            text = common_ask.com29
            check = True
        elif data == 31:
            text = kass.kass31
            check = True
        elif data == 32:
            text = kass.kass32
            check = True
        elif data == 33:
            text = kass.kass33
            check = True
        elif data == 34:
            text = kass.kass34
            check = True
        elif data == 35:
            text = kass.kass35
            check = True
        elif data == 41:
            text = onec.c1
            check = False
            bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                  message_id=query.message.message_id, reply_markup=onec.reply_c1_kb)
        elif data == 411:
            text = onec.c411
            check = True
        elif data == 412:
            text = onec.c412
            check = True
        elif data == 413:
            text = onec.c413
            check = True
        elif data == 42:
            text = onec.c42
            check = True
        elif data == 43:
            text = onec.c43
            check = True
        elif data == 44:
            text = onec.c44
            check = True
        elif data == 45:
            text = onec.c45
            check = True
        elif data == 46:
            text = onec.c46
            check = True
        elif data == 47:
            text = onec.c47
            check = True
        elif data == 48:
            text = onec.c48
            check = True
        elif data == 49:
            text = onec.c49
            check = False
            bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                  message_id=query.message.message_id, reply_markup=onec.reply_c49_kb)
        elif data == 4051:
            text = onec.maintext
            check = False
            bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                  message_id=query.message.message_id, reply_markup=onec.reply_i_kb_ones1)
        elif data == 4052:
            text = onec.maintext
            check = False
            bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                  message_id=query.message.message_id, reply_markup=onec.reply_i_kb_ones2)
        elif data == 4053:
            text = onec.maintext
            check = False
            bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                  message_id=query.message.message_id, reply_markup=onec.reply_i_kb_ones3)
        elif data == 51:
            if query.message.chat.type == 'private':
                text = agent.a51
                check = False
                bot.send_message(query.message.chat.id, text=text)
            else:
                text = agent.a51off
                check = False
                bot.send_message(query.message.chat.id, text=text)
        elif data == 52:
            text = agent.a52
            check = True
        elif data == 53:
            text = agent.a53
            check = True
        elif data == 54:
            text = agent.a54
            check = False
            bot.send_message(query.message.chat.id, text=text)
            bot.send_document(text=text, chat_id=query.message.chat.id, document=open('files/repair_scaner.docx', 'rb'))
        elif data == 55:
            text = agent.a55
            check = True
        elif data == 56:
            text = agent.a56
            check = True
        elif data == 57:
            text = agent.a54
            check = False
            bot.send_message(query.message.chat.id, text=text)
            bot.send_document(text=text, chat_id=query.message.chat.id, document=open('files/clear_phone.docx', 'rb'))
        elif data == 58:
            text = agent.a58
            check = True
        elif data == 59:
            text = agent.a59
            check = True
        elif data == 592:
            text = agent.a592
            check = True
        elif data == 591:
            text = agent.a591
            check = True
        elif data == 61:
            text = goodmood.gm61
            check = False
            bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                  message_id=query.message.message_id, reply_markup=goodmood.reply_i_gm61)
        elif data == 6101:
            text = get_cod(1)
            bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                  message_id=query.message.message_id, reply_markup=reply_i_kb_back)
            check = False
        elif data == 6102:
            text = get_cod(2)
            check = False
            bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                  message_id=query.message.message_id, reply_markup=reply_i_kb_back)
        elif data == 6103:
            text = get_cod(3)
            check = False
            bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                  message_id=query.message.message_id, reply_markup=reply_i_kb_back)
        elif data == 6104:
            text = get_cod(4)
            check = False
            bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                  message_id=query.message.message_id, reply_markup=reply_i_kb_back)
        elif data == 6105:
            text = get_cod(5)
            check = False
            bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                  message_id=query.message.message_id, reply_markup=reply_i_kb_back)
        elif data == 6106:
            text = get_cod(6)
            check = False
            bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                  message_id=query.message.message_id, reply_markup=reply_i_kb_back)
        elif data == 6107:
            text = get_cod(7)
            check = False
            bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                  message_id=query.message.message_id, reply_markup=reply_i_kb_back)
        elif data == 6108:
            text = get_cod(8)
            check = False
            bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                  message_id=query.message.message_id, reply_markup=reply_i_kb_back)
        elif data == 6109:
            text = get_cod(9)
            check = False
            bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                  message_id=query.message.message_id, reply_markup=reply_i_kb_back)
        elif data == 6110:
            text = get_cod(10)
            check = False
            bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                  message_id=query.message.message_id, reply_markup=reply_i_kb_back)
        elif data == 491:
            text = onec.c491
            check = True
        elif data == 492:
            text = onec.c492
            check = True
        elif data == 4010:
            text = onec.c4010
            check = True
        elif data == 4011:
            text = onec.c4011
            check = True
        elif data == 4012:
            text = onec.c4012
            check = True
        elif data == 4013:
            text = onec.c4013
            check = True
        elif data == 4014:
            text = onec.c4014
            check = True
        elif data == 4015:
            text = onec.c4015
            check = True
        elif data == 4016:
            text = onec.c4016
            check = True
        elif data == 4017:
            text = onec.c4017
            check = True
        elif data == 400:
            text = onec.maintext
            check = False
            bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                  message_id=query.message.message_id, reply_markup=onec.reply_i_kb_ones1)
        elif data == 999:
            text = main_page.maintext
            check = False
            if query.message.chat.type == 'private':
                bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                  message_id=query.message.message_id, reply_markup=main_page.reply_main_kb)
            else:
                bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                      message_id=query.message.message_id, reply_markup=main_page.reply_main_kb1)
        elif data == 300:
            text = common_ask.maintext
            check = False
            bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                  message_id=query.message.message_id, reply_markup=common_ask.reply_i_kb_main)
        else:
            text = common_ask.maintext
            check = False
            if query.message.chat.type == 'private':
                bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                  message_id=query.message.message_id, reply_markup=main_page.reply_main_kb)
            else:
                bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                                      message_id=query.message.message_id, reply_markup=main_page.reply_main_kb)
    except TypeError:
        text = "Что-то пошло не так"

    if check:
        bot.edit_message_text(text=text, chat_id=query.message.chat.id,
                          message_id=query.message.message_id, reply_markup=reply_i_kb_back, parse_mode=ParseMode.MARKDOWN)