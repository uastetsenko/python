from threading import Thread, enumerate
from urllib.request import urlopen
from functools import reduce
from time import sleep
import json
from usps import USPSApi

UPDATE_INTERVAL = 0.01

class URLThread(Thread):
    def __init__(self,tr):
        super(URLThread, self).__init__()
        self.tr = tr
        self.delivered = False
        self.zip_code = ''
        self.delivered_time = ''
        self.track_number = ''
        self.error = False

    def run(self):
        usps = USPSApi('070SELLE2158')
        track = usps.track(self.tr)

        json_data = json.dumps(track.result)
        end_str = json.loads(json_data)

        self.track_number = end_str.get('TrackResponse').get('TrackInfo').get('@ID')

        try:
            status = end_str.get('TrackResponse').get('TrackInfo').get('TrackSummary').get('Event')
            if 'elivered' in status:
                self.delivered = True

                self.zip_code = end_str.get('TrackResponse').get('TrackInfo').get('TrackSummary').get('EventZIPCode')
                d_date = end_str.get('TrackResponse').get('TrackInfo').get('TrackSummary').get('EventDate')
                d_time = end_str.get('TrackResponse').get('TrackInfo').get('TrackSummary').get('EventTime')
                self.delivered_time = d_date + " at " + d_time

        except AttributeError:
            self.error = True


def multi_get(uris,timeout=2.0):
    def alive_count(lst):
        alive = map(lambda x : 1 if x.isAlive() else 0, lst)
        return reduce(lambda a,b : a + b, alive)
    threads = [ URLThread(uri) for uri in uris ]
    for thread in threads:
        thread.start()
    while alive_count(threads) > 0 and timeout > 0.0:
        timeout = timeout - UPDATE_INTERVAL
        sleep(UPDATE_INTERVAL)
    return [(x.track_number, x.delivered, x.zip_code, x.delivered_time, x.error) for x in threads]

if __name__ == '__main__':
    tr = ['RH612342533UA', 'RA691234558UA', 'RA06423455UA', 'RH612342533UA', 'RA691234558UA', 'RA06423455UA' ]

    info_about_track = multi_get(tr,timeout=1.5)
    a = 0
    for track_number, delivered, zip_code, delivered_time, error in info_about_track:
        if error:
            print("%s (%s) - Wront TrackNumber" % (str(a), track_number))
        elif delivered:
            print("%s (%s) - %s in %s" % (str(a), track_number, str(delivered), str(delivered_time)))
        else:
            print("%s (%s) - %s" % (str(a), track_number, str(delivered)))
        a += 1
