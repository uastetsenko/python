from default import *


class ParseOG(Parser):
    def __init__(self, url):
        self.url = url

    def get_info_in_page(self):
        i_title = ''
        i_url = ''
        i_shop_name = 'Shop'
        i_color = []
        i_size = []
        i_photo = []
        i_currency_good = 'USD'
        i_cost_good = 0.01
        i_country = 'USA'
        i_sku = '0'
        i_enum = 'Shop'
        og = {}

        try:
            og = OpenGraph(url=self.url, timeout=5)
            try:
                i_title = og.title
            except (AttributeError, TypeError):
                pass

            try:
                i_shop_name = og.site_name
            except (AttributeError, TypeError):
                pass

            try:
                i_photo.append(og.image)
            except (AttributeError, TypeError):
                pass
        except (AttributeError, TypeError):
            pass

        if len(str(og)) == 2:
            try:
                html = self.get_html(self.url)
                soup = BeautifulSoup(html, 'lxml')
                try:
                    i_title = soup.find('div', class_='response-errors').text
                except:
                    pass

                try:
                    i_title = soup.find("meta", property="og:title")['content']
                except (AttributeError, TypeError, KeyError):
                    pass

                try:
                    i_photo_bad = soup.find("meta", property="og:image")['content']
                    i_photo.append(i_photo_bad)
                except (AttributeError, TypeError, KeyError):
                    pass
            except (AttributeError, TypeError, KeyError):
                pass

        if i_title == '' and i_shop_name == 'Shop':
            end_json = ParseSchemaOrg(self.url).get_info_in_page()
            return end_json
        else:
            end_json = self.go_to_json(i_enum, i_title, i_sku, i_shop_name, i_cost_good, i_currency_good,
                                       i_photo, i_color, i_size, self.url, i_country)
            return end_json

class ParseSchemaOrg(Parser):
    def __init__(self, url):
        self.url = url

    def get_info_in_page(self):
        i_title = 'Not any parser'
        i_shop_name = 'Shop'
        i_color = []
        i_size = []
        i_photo = []
        i_currency_good = 'USD'
        i_cost_good = 0.01
        i_country = 'USA'
        i_sku = '0'
        i_enum = 'Shop'
        it_checker = False

        try:
            items = microdata.get_items(urllib.request.urlopen(self.url, cafile=certifi.where(), timeout=5))
            item = items[0]

            if 'Product' in str(item.itemtype):
                it_checker = True
            else:
                try:
                    item = items[1]
                    it_checker = True
                except:
                    pass

            if it_checker:
                try:
                    i_title = item.name
                except (AttributeError, TypeError):
                    pass

                try:
                    dom = self.url.split("//")[-1].split("/")[0]
                    dom = dom.replace("www.", "")
                    i_shop_name = dom
                except (AttributeError, TypeError):
                    pass

                try:
                    i_cost_bad = item.price
                    i_currency_good = self.get_currency(i_cost_bad)[0]
                    i_cost_good = self.get_currency(i_cost_bad)[1]
                except (AttributeError, TypeError):
                    try:
                        i_cost_good = item.offers.price
                        try:
                            i_currency_good = item.offers.priceCurrency
                            if '£' in i_currency_good:
                                i_currency_good = 'GBR'
                            elif '€' in i_currency_good:
                                i_currency_good = 'EUR'
                            else:
                                i_currency_good = 'USD'
                        except (AttributeError, TypeError):
                            pass
                    except (AttributeError, TypeError):
                        pass
            else:
                pass

        except:
            i_title = 'ConnectionError'
            # 99% Здесь Таймауты, Редиректы и прочая ерунда, проще пропустить все чем отлавливать по отдельности

        try:
            float(i_cost_good)
        except (TypeError, ValueError):
            i_cost_good = 0.01

        end_json = self.go_to_json(i_enum, i_title, i_sku, i_shop_name, i_cost_good, i_currency_good,
                                   i_photo, i_color, i_size, self.url, i_country)
        return end_json
