from default import *

class ParserVS(Parser):
    def __init__(self, url):
        self.url = url

    def get_info_in_page(self):
        html = self.get_html(self.url)
        i_shop_name = 'Victorias Secret'
        i_title = ''
        i_color = []
        i_size = []
        i_photo = []
        i_currency_good = 'USD'
        i_cost_good = 0.01
        i_country = 'USA'
        i_sku = '0'
        i_enum = 'Shop'

        soup = BeautifulSoup(html, 'lxml')

        try:
            i_title = soup.find('div', class_='response-errors').text
        except:
            pass

        try:
            b_text = soup.find('h1').text.strip()
            b_text = b_text.replace('\n', '')
            i_title = b_text.replace('  ', '')
            if 'NEW!' in i_title:
                i_title = i_title.replace('NEW!', ' NEW!')
        except (AttributeError, TypeError):
            pass

        try:
            i_sku = soup.find('p', class_='itemNbr').text
        except (AttributeError, TypeError):
            pass

        try:
            i_photo_bad = soup.find('img', id='vsImage')['src']
            i_photo_bad = i_photo_bad.replace('//', 'https://')
            i_photo.append(i_photo_bad)
        except (AttributeError, TypeError):
            pass

        try:
            pattern = re.compile(r'UU.save(.*?)}')
            gg = soup.find_all('script', text=pattern)
            try:
                bad_temp = re.search(u'itemPrice", \[{(.*)}]\);', str(gg)).group(1)
                try:
                    i_size_bad = re.search(u'sizeCode(.*)colorCode', str(bad_temp)).group(1)
                    i_size = i_size_bad.replace('":[', '').replace('"],', '').replace('"', '').replace(',',' ').split()
                except (AttributeError, TypeError):
                    pass

                try:
                    i_color_bad = re.search(u'colorCode(.*)breakQty', str(bad_temp)).group(1)
                    i_color = i_color_bad.replace('":[', '').replace('"],', '').replace('"', '').replace(',', ' ').split()
                    # Можно получить название цвета лишь кодом. Прм. FHS, 68S)
                except (AttributeError, TypeError):
                    pass

                try:
                    i_cost_bad = re.search(u'origPriceDisplay(.*)dealAmount', str(bad_temp)).group(1)
                    i_cost_bad2 = i_cost_bad.replace('":"', '').replace('","', '').replace(' ', '').replace(',', '.')
                    i_currency_good = self.get_currency(i_cost_bad2)[0]
                    i_cost_good = self.get_currency(i_cost_bad2)[1]
                except (AttributeError, TypeError):
                    pass
            except (AttributeError, TypeError):
                pass
        except (AttributeError, TypeError):
            pass

        end_json = self.go_to_json(i_enum, i_title, i_sku, i_shop_name, i_cost_good, i_currency_good,
                                   i_photo, i_color, i_size, self.url, i_country)
        return end_json
