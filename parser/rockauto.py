from default import *

class ParserRockauto(Parser):
    def __init__(self, url):
        self.url = url

    def get_info_in_page(self):
        html = self.get_html(self.url)
        i_shop_name = 'Rockauto'
        i_title = ''
        i_color = []
        i_size = []
        i_photo = []
        i_currency_good = 'USD'
        i_cost_good = 0.01
        i_country = 'USA'
        i_sku = '0'
        i_enum = 'Shop'

        soup = BeautifulSoup(html, 'lxml')

        try:
            i_title = soup.find('div', class_='response-errors').text
        except:
            pass

        try:
            i_title_bad = soup.find('div', class_='bdytext hide-for-inline-mobile').find('table').find('td').text
            i_title = i_title_bad.replace('More Information for ', '')
        except (AttributeError, TypeError):
            pass

        try:
            i_photo_bad = soup.find('div', class_='inlineimg_containersizer').find('img')
            i_photo_bad2 = i_photo_bad['src'].replace('/info', 'https://www.rockauto.com/info')
            i_photo.append(i_photo_bad2)
        except (AttributeError, TypeError):
            pass

        # Остальные данные нельзя вытащить со страницы товара, их там попросту НЕТ

        end_json = self.go_to_json(i_enum, i_title, i_sku, i_shop_name, i_cost_good, i_currency_good,
                                   i_photo, i_color, i_size, self.url, i_country)

        return end_json
