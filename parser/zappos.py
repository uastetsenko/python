from default import *

class Parser6pm(Parser):
    def __init__(self, url):
        self.url = url

    def get_info_in_page(self):
        html = self.get_html(self.url)
        i_title = ''
        i_color = []
        i_size = []
        i_photo = []
        i_currency_good = 'USD'
        i_cost_good = 0.01
        i_country = 'USA'
        i_sku = '0'
        i_enum = 'Shop'

        soup = BeautifulSoup(html, 'lxml')

        try:
            i_title = soup.find('div', class_='response-errors').text
        except:
            pass

        try:
            b_text = soup.find('title').text
            i_title = b_text[:b_text.find(' at')]
        except (AttributeError, TypeError):
            pass

        if '6pm.com' in self.url:
            i_shop_name = '6PM'
        else:
            i_shop_name = 'Zappos'

        try:
            i_sku = soup.find('div', class_='_3iH0n').find('li').text[6:]
        except (AttributeError, TypeError):
            pass

        try:
            i_cost_bad = soup.find('span', class_='_3r_Ou').text
            i_currency_good = self.get_currency(i_cost_bad)[0]
            i_cost_good = self.get_currency(i_cost_bad)[1]
        except (AttributeError, TypeError):
            pass

        try:
            i_color_bad = soup.find('select', id='pdp-color-select').find_all('option')
            for a in i_color_bad:
                i_color.append(a.text)
        except (AttributeError, TypeError):
            try:
                i_color_bad = soup.find('div', class_='_2BNsB').find('div').text
            except (AttributeError, TypeError):
                pass

        try:
            i_size_bad = soup.find('select', id='pdp-size-select').find_all('option')
            for a in i_size_bad:
                if 'ize' in a.text: continue
                i_size.append(a.text)
        except (AttributeError, TypeError):
            try:
                i_size_bad = soup.find('div', class_='_3EJdc').find('div', class_='_2I2pe').text
                i_size.append(i_size_bad)
            except (AttributeError, TypeError):
                pass

        try:
            i_photo_bad = soup.find('div', id='thumbnailsList').find_all('img')
            for a in i_photo_bad:
                b_link = a['src']
                g_link = b_link.replace('._SR106,78_', '')
                i_photo.append(g_link)
        except (AttributeError, TypeError):
            pass

        end_json = self.go_to_json(i_enum, i_title, i_sku, i_shop_name, i_cost_good, i_currency_good,
                                   i_photo, i_color, i_size, self.url, i_country)
        return end_json
