from default import *

class ParserIherb(Parser):
    def __init__(self, url):
        self.url = url

    def get_info_in_page(self):
        html = self.get_html(self.url)
        i_shop_name = 'iHerb'
        i_title = ''
        i_color = []
        i_size = []
        i_photo = []
        i_currency_good = 'USD'
        i_cost_good = 0.01
        i_country = 'US'
        i_sku = '0'
        i_enum = 'Shop'

        soup = BeautifulSoup(html, 'lxml')

        try:
            i_title = soup.find('div', class_='response-errors').text
        except:
            pass

        try:
            b_text = soup.find('title').text
            i_title = b_text.replace(' - iHerb.com', '')
        except (AttributeError, TypeError):
            pass

        try:
            i_sku = soup.find('span', itemprop='gtin12').text
        except (AttributeError, TypeError):
            pass

        try:
            i_cost_bad = soup.find('div', id='price').text.strip()
            i_currency_good = self.get_currency(i_cost_bad)[0]
            i_cost_good = self.get_currency(i_cost_bad)[1]
        except (AttributeError, TypeError):
            pass

        try:
            i_photo_bad = soup.find('div', class_='thumbnail-container').find_all('img')
            for a in i_photo_bad:
                if '/t/' in a['src']:
                    b_temp = a['src'].replace('/t/', '/l/')
                    i_photo.append(b_temp)
                else:
                    b_temp = a['src'].replace('/t/', '/v/')
                    i_photo.append(b_temp)
        except (AttributeError, TypeError):
            pass

        end_json = self.go_to_json(i_enum, i_title, i_sku, i_shop_name, i_cost_good, i_currency_good,
                                   i_photo, i_color, i_size, self.url, i_country)

        return end_json
