from default import *

class ParserTowerhobbies(Parser):
    def __init__(self, url):
        self.url = url

    def get_info_in_page(self):
        html = self.get_html(self.url)
        i_shop_name = 'Tower Hobbies'
        i_title = ''
        i_color = []
        i_size = []
        i_photo = []
        i_currency_good = 'USD'
        i_cost_good = 0.01
        i_country = 'USA'
        i_sku = '0'
        i_enum = 'Shop'

        soup = BeautifulSoup(html, 'lxml')

        try:
            i_title = soup.find('div', class_='response-errors').text
        except:
            pass

        try:
            i_title = soup.find('h1').text
        except (AttributeError, TypeError):
            pass

        try:
            i_sku = soup.find('span', class_='stocknum').text
            i_sku = i_sku[i_sku.find('#'):].replace('# ', '').strip()
        except (AttributeError, TypeError):
            pass

        try:
            i_cost_bad = soup.find('div', class_='hidden-xs hidden-sm towprice').text
            i_currency_good = self.get_currency(i_cost_bad)[0]
            i_cost_good = self.get_currency(i_cost_bad)[1]
        except (AttributeError, TypeError):
            pass

        try:
            i_photo_bad = soup.find('div', class_='mainproductimgslider').find_all('img')
            for a in i_photo_bad:
                i_photo.append(a['src'])
        except (AttributeError, TypeError):
            pass

        end_json = self.go_to_json(i_enum, i_title, i_sku, i_shop_name, i_cost_good, i_currency_good,
                                   i_photo, i_color, i_size, self.url, i_country)

        return end_json
