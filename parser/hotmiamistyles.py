from default import *

class Parsehotmiamistyles(Parser):
    def __init__(self, url):
        self.url = url

    def get_info_in_page(self):
        html = self.get_html(self.url)
        i_shop_name = 'Hot Miami Styles'
        i_title = ''
        i_color = []
        i_size = []
        i_photo = []
        i_currency_good = 'USD'
        i_cost_good = 0.01
        i_country = 'USA'
        i_sku = '0'
        i_enum = 'Shop'

        soup = BeautifulSoup(html, 'lxml')

        try:
            i_title = soup.find('div', class_='response-errors').text
        except:
            pass

        try:
            i_title = soup.find('font', 'productnamecolorLARGE colors_productname').find('span').text
        except (AttributeError, TypeError):
            pass

        try:
            i_sku = soup.find('span', 'product_code').text
        except (AttributeError, TypeError):
            pass

        try:
            i_cost_bad = soup.find('div', class_='product_productprice').text.strip()
            i_currency_good = self.get_currency(i_cost_bad)[0]
            i_cost_good = self.get_currency(i_cost_bad)[1]
        except (AttributeError, TypeError):
            pass

        try:
            i_photo_bad = soup.find('span', id='altviews').find_all('img')
            for a in i_photo_bad:
                p_temp = a['src'].replace('//', 'https://').replace('S.jpg', 'jpg').replace('S.png', '.png')
                i_photo.append(p_temp[:p_temp.find('?')])
        except (AttributeError, TypeError):
            pass

        try:
            i_size_bad = soup.find('script', language="JavaScript")
            i_size_bad2 = re.findall(r'TCN_addContent(.*);', str(i_size_bad))
            for a in i_size_bad2:
                s_temp = a[:a.find('+')].replace('("', '')
                i_size.append(s_temp)
        except (AttributeError, TypeError):
            pass

        end_json = self.go_to_json(i_enum, i_title, i_sku, i_shop_name, i_cost_good, i_currency_good,
                                   i_photo, i_color, i_size, self.url, i_country)

        return end_json