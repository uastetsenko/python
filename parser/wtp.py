from default import *
from ebay import ParserEbay
from amazon import ParserAmazon
from autopartswarehouse import ParserAutopartsWarehouse
from hotmiamistyles import Parsehotmiamistyles
from iherb import ParserIherb
from northenautoparts import ParserNorthernautoparts
from ralphlauren import ParserRalphlauren
from rockauto import ParserRockauto
from sephora import ParserSephora
from towerhobbies import ParserTowerhobbies
from universal import ParseOG, ParseSchemaOrg
from vs import ParserVS
from zappos import Parser6pm

class WhatTheParser(Parser):
    def ident(self, url):
        if 'ebay' in url:
            return ParserEbay(url).get_info_in_page()
        elif '6pm' in url or 'zappos' in url:
            return Parser6pm(url).get_info_in_page()
        elif 'amazon' in url:
            return ParserAmazon(url).get_info_in_page()
        elif 'iherb' in url:
            return ParserIherb(url).get_info_in_page()
        elif 'victoriassecret' in url:
            return ParserVS(url).get_info_in_page()
        elif 'northernautoparts' in url:
            return ParserNorthernautoparts(url).get_info_in_page()
        elif 'sephora' in url:
            return ParserSephora(url).get_info_in_page()
        elif 'ralphlauren' in url:
            return ParserRalphlauren(url).get_info_in_page()
        elif 'autopartswarehouse' in url:
            return ParserAutopartsWarehouse(url).get_info_in_page()
        elif 'towerhobbies' in url:
            return ParserTowerhobbies(url).get_info_in_page()
        elif 'hotmiamistyles' in url:
            return Parsehotmiamistyles(url).get_info_in_page()
        elif 'rockauto' in url:
            return ParserRockauto(url).get_info_in_page()
        else:
            return ParseOG(url).get_info_in_page()


def main():
    regex = re.compile(
        r'^(?:http|ftp)s?://'  # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
        r'localhost|'  # localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
        r'(?::\d+)?'  # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)

    url = 'https://www.ttt.ua/shop/product/smartfon-honor-8x-blue'
    if re.match(regex, url)is not None:
        link = url.strip().encode('ascii', errors='ignore').decode()
        print(WhatTheParser().ident(link))

if __name__ == '__main__':
    main()