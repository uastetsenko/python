from default import *

class ParserEbay(Parser):
    def __init__(self, url):
        self.url = url

    def get_info_in_page(self):
        html = self.get_html(self.url)
        i_title = ''
        i_shop_name = 'Ebay'
        i_color = []
        i_size = []
        i_photo = []
        i_currency_good = 'USD'
        i_cost_good = 0.01
        i_country = ''
        i_sku = '0'
        i_enum = 'Shop'

        if 'ebay.com' in self.url:
            i_country = 'USA'
        elif 'ebay.de' in self.url:
            i_country = 'DE'
        else:
            i_country = 'UK'

        soup = BeautifulSoup(html, 'lxml')

        try:
            i_title = soup.find('div', class_='response-errors').text
        except:
            pass

        try:
            b_text = str(soup.find('h1', id='itemTitle').text)
            i_title = re.sub(r'\s+', ' ', b_text).strip()
            i_title = i_title.replace("Details about ", "").replace("Details zu ", "")
        except (AttributeError, TypeError):
            pass

        try:
            i_cost_bad = soup.find('span', id='prcIsum').text
            i_enum = 'EBAY_own'
        except (AttributeError, TypeError):
            try:
                i_cost_bad = soup.find('span', id='mm-saleDscPrc').text
                i_enum = 'EBAY_buyitnow'
            except (AttributeError, TypeError):
                try:
                    i_cost_bad = soup.find('span', id='prcIsum_bidPrice').text
                    i_enum = 'EBAY_autction'
                except (AttributeError, TypeError):
                    i_enum = 'Shop'
                    i_cost_bad = "US $1"

        try:
            i_currency_good = self.get_currency(i_cost_bad)[0]
            i_cost_good = self.get_currency(i_cost_bad)[1]
        except (AttributeError, TypeError):
            pass

        try:
            i_photo_bad = soup.find('div', id='vi_main_img_fs').find_all('img')
            for a in i_photo_bad:
                p_temp = a['src'].replace('s-l64', 's-l500')
                i_photo.append(p_temp)
        except (AttributeError, TypeError):
            try:
                i_photo_bad = soup.find('div', id='mainImgHldr').find_all('img', id='icImg')
                for a in i_photo_bad:
                    p_temp = a['src'].replace('s-l64', 's-l500').replace('s-l300', 's-l500')
                    i_photo.append(p_temp)
            except (AttributeError, TypeError):
                pass

        try:
            i_color_bad = soup.find_all('div', id='test12')
            if len(i_color_bad) == 1:
                b_temp = i_color_bad[0].find('select').get('name')
                if "ize" in b_temp:
                    for b in i_color_bad[0].find_all('option'):
                        if 'elect' in b.text:
                            continue
                        i_size.append(b.text)
                elif "olor" in b_temp or "olour" in b_temp:
                    for b in i_color_bad[0].find_all('option'):
                        if 'elect' in b.text:
                            continue
                        i_color.append(b.text)
                else:
                    pass
            else:
                for a in range(len(i_color_bad)):
                    b_temp = i_color_bad[a].find('select').get('name')
                    if "ize" in b_temp:
                        for b in i_color_bad[a].find_all('option'):
                            if 'elect' in b.text:
                                continue
                            i_size.append(b.text)
                    elif "olor" in b_temp or "olour" in b_temp:
                        for b in i_color_bad[a].find_all('option'):
                            if 'elect' in b.text:
                                continue
                            i_color.append(b.text.replace('[', '(').replace(']', ')'))
                    else:
                        pass
        except (AttributeError, TypeError):
            pass

        end_json = self.go_to_json(i_enum, i_title, i_sku, i_shop_name, i_cost_good, i_currency_good,
                                   i_photo, i_color, i_size, self.url, i_country)
        return end_json
