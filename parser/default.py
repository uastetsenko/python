import requests
import certifi
from bs4 import BeautifulSoup
import json
import re
from opengraph import OpenGraph
import microdata
import urllib.request
from jsonschema import validate, Draft3Validator, exceptions


class Parser:
    def get_html(self, url):
        headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
        try:
            response = requests.get(url, headers=headers, timeout=5)
            if response.status_code == 404:
                return '<html><div class="response-errors">404</div></html>'
            else:
                return response.text
        except requests.exceptions.ConnectionError:
            return '<html><div class="response-errors">ConnectionError</div></html>'
        except requests.exceptions.MissingSchema:
            return '<html><div class="response-errors">MissingSchema</div></html>'
        except requests.exceptions.ReadTimeout:
            return '<html><div class="response-errors">ReadTimeout</div></html>'
        except requests.exceptions.TooManyRedirects:
            return '<html><div class="response-errors">TooManyRedirects</div></html>'

    def get_currency(self, bad_data):
        i_currency_good = 'USD'
        i_cost_good = 0.01

        bad_data = bad_data.replace(',', '.').replace('N\A', '')
        if '-' in bad_data:
            bad_data = bad_data[:bad_data.find(' - ')]

        if 'US' in bad_data or 'C' in bad_data or '$' in bad_data:
            i_currency_good = 'USD'
            i_cost_good = bad_data.replace("US $", "").replace("C $", "").replace("$", "")\
                .replace("C", "").replace("US", "")

        elif 'EUR' in bad_data or '€' in bad_data:
            i_currency_good = 'EUR'
            i_cost_good = bad_data.replace("EUR", "").replace("€", "").strip()
        elif 'GBP' in bad_data or '£' in bad_data:
            i_currency_good = 'GBR'
            i_cost_good = bad_data.replace("GBP", "").replace("£", "").strip()
        elif "₴" in bad_data:
            i_cost_good = bad_data.replace('₴', '')
            i_currency_good = 'UAH'
        elif 'CAD' in bad_data:
            i_currency_good = 'CAD'
            i_cost_good = bad_data.replace('CAD', '')
        else:
            pass

        return [i_currency_good, i_cost_good]

    def validate(self, ret_object):
        parsed_string = json.loads(ret_object)

        good_schema = {
            'title': 'Remote good schema',
            'type': 'object',
            'properties': {
                'order_type': {
                    'description': 'Type of order',
                    'type': "string",
                    'enum': [
                        'Shop', 'EBAY_buyitnow', 'EBAY_autction', 'EBAY_own'
                    ],
                    'default': 'Shop'

                },
                "url": {
                    'description': 'URL of ordered good',
                    "additionalProperties": {
                        "type": "string",
                        "format": "uri"
                    }
                },
                "title": {
                    'description': 'title of good',
                    'type': "string"
                },
                "sku": {
                    'description': 'sku of good',
                    'type': "string"
                },
                "shop_name": {
                    'description': 'Shop name',
                    'type': "string"
                },
                "cost": {
                    'description': 'cost of good',
                    'type': "number",
                    'minimum': 0.01
                },
                'currency': {
                    'description': 'Shop name',
                    'type': "string",
                    'enum': [
                        'USD', 'GBR', 'EUR', 'CAD', 'UAH'
                    ],
                    'default': 'USD'
                },
                'photo': {
                    'description': 'photos of the good',
                    'type': 'array',
                    'items': {
                        "additionalProperties": {
                            "type": "string",
                            "format": "uri"
                        }
                    }
                },
                'country': {
                    'description': 'ISO2 code of country',
                    'type': "string",
                    'default': ''
                },
                "color": {
                    'description': 'List of colors',
                    'type': "array",
                    "items": {
                        "type": "string"
                    }
                },
                "size": {
                    'description': 'List of sizes',
                    'type': "array",
                    "items": {
                        "type": "string"
                    }
                },
                'success': {
                    'description': 'ok or not',
                    'type': 'boolean'
                }
            },
            'required': [
                'url',
                'title',
                'shop_name'
            ]
        }

        try:
            validate(parsed_string, good_schema)
            return True, ''
        except exceptions.ValidationError:
            v = Draft3Validator(good_schema)
            for error in sorted(v.iter_errors(parsed_string), key=str):
                print(error.message)
                return False, error.message

    def go_to_json(self, i_enum, title, sku, shop_name, cost, currency, photo, color, size, url, country):
        if i_enum == '':
            i_enum = 'Shop'
        try:
            title = title.replace('™', '').replace('®', '')
        except (AttributeError, TypeError):
            pass

        data = {
            'order_type': i_enum,
            'url': url,
            'title': str(title),
            'sku': str(sku),
            'shop_name': shop_name,
            'cost': float(cost),
            'currency': str(currency),
            'photo': photo,
            'country': country,
            'color': color,
            'size': size,
        }

        json_data = json.dumps(data, ensure_ascii=False)
        (check, err_message) = self.validate(json_data)

        if title == 'ConnectionError':
            data['success'] = False
            data['error_message'] = 'ConnectionError'
            return json.dumps(data, ensure_ascii=False)
        elif title == '404':
            data['success'] = False
            data['error_message'] = '404'
            return json.dumps(data, ensure_ascii=False)
        elif title == 'ReadTimeout':
            data['success'] = False
            data['error_message'] = 'ReadTimeout'
            return json.dumps(data, ensure_ascii=False)
        elif title == 'TooManyRedirects':
            data['success'] = False
            data['error_message'] = 'TooManyRedirects'
            return json.dumps(data, ensure_ascii=False)
        elif title == 'MissingSchema':
            data['success'] = False
            data['error_message'] = 'MissingSchema'
            return json.dumps(data, ensure_ascii=False)
        else:
            if check is True:
                data['success'] = True
                return json.dumps(data, ensure_ascii=False)
            else:
                data['success'] = False
                data['error_message'] = err_message
                return json.dumps(data, ensure_ascii=False)
