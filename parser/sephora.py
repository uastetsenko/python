from default import *

class ParserSephora(Parser):
    def __init__(self, url):
        self.url = url

    def get_info_in_page(self):
        html = self.get_html(self.url)
        i_shop_name = 'Sephora'
        i_title = ''
        i_color = []
        i_size = []
        i_photo = []
        i_currency_good = 'USD'
        i_cost_good = 0.01
        i_country = 'USA'
        i_sku = '0'
        i_enum = 'Shop'

        soup = BeautifulSoup(html, 'lxml')

        try:
            i_title = soup.find('div', class_='response-errors').text
        except:
            pass

        try:
            b_text1 = soup.find('span', class_='css-r4ddnb').text
            b_text2 = soup.find('span', class_='css-15zphjk').text
            i_title = b_text1 + ' - ' + b_text2
        except (AttributeError, TypeError):
            pass

        try:
            i_sku = soup.find('div', class_='css-altys').text
            i_sku = i_sku[i_sku.find('ITEM'):].replace('ITEM ', '')
        except (AttributeError, TypeError):
            pass

        try:
            i_cost_bad = soup.find('div', class_='css-n8yjg7').text
            if '(' and ')' in i_cost_bad:
                i_cost_bad2 = i_cost_bad[:i_cost_bad.find('(')]
            else:
                i_cost_bad2 = i_cost_bad

            i_currency_good = self.get_currency(i_cost_bad2)[0]
            i_cost_good = self.get_currency(i_cost_bad2)[1]
        except (AttributeError, TypeError):
            pass

        try:
            i_photo_bad = soup.find_all('img', class_='css-z2rv3f')
            for a in i_photo_bad:
                t_photo = a['src'].replace('-thumb50','-Lhero').replace('-thumb-50', '-Lhero').replace('/productimages', 'https://www.sephora.com/productimages')
                i_photo.append(t_photo)
        except (AttributeError, TypeError):
            pass

        try:
            i_color_bad = soup.find('div', class_='css-gth5yg ').find_all('div', class_='css-1ax77m2')
            for a in i_color_bad:
                i_color.append(a.find('div')['aria-label'])
        except (AttributeError, TypeError):
            pass

        end_json = self.go_to_json(i_enum, i_title, i_sku, i_shop_name, i_cost_good, i_currency_good,
                                   i_photo, i_color, i_size, self.url, i_country)

        return end_json
