from default import *

class ParserAmazon(Parser):
    def __init__(self, url):
        self.url = url

    def get_info_in_page(self):
        html = self.get_html(self.url)
        i_title = ''
        i_shop_name = 'Amazon'
        i_color = []
        i_size = []
        i_photo = []
        i_currency_good = 'USD'
        i_cost_good = 0.01
        i_country = 'USA'
        i_sku = '0'
        i_enum = 'Shop'

        if 'amazon.com' in self.url:
            i_country = 'USA'
        elif 'amazon.it' in self.url:
            i_country = 'IT'
        elif 'amazon.de' in self.url:
            i_country = 'GE'

        soup = BeautifulSoup(html, 'lxml')

        try:
            i_title = soup.find('div', class_='response-errors').text
        except:
            pass

        try:
            b_text = str(soup.find('span', id='productTitle').text)
            i_title = re.sub(r'\s+', ' ', b_text).strip()
        except (AttributeError, TypeError):
            try:
                b_text = str(soup.find('h1', id='title').text)
                i_title = re.sub(r'\s+', ' ', b_text).strip()
            except (AttributeError, TypeError):
                pass

        try:
            i_cost_bad = soup.find('span', id='priceblock_ourprice').text
        except (AttributeError, TypeError):
            try:
                i_cost_bad = soup.find('span', id='buyingPriceValue').text
            except (AttributeError, TypeError):
                try:
                    i_cost_bad = soup.find('span', id='priceblock_saleprice').text
                except (AttributeError, TypeError):
                    try:
                        i_cost_bad = soup.find('span', id='priceblock_dealprice').text
                    except (AttributeError, TypeError):
                        try:
                            i_cost_bad = soup.find('span', class_='a-size-mini twisterSwatchPrice').text.strip()
                        except (AttributeError, TypeError):
                            try:
                                i_cost_bad = soup.find('span', class_='a-size-base a-color-price a-color-price').text.strip()
                            except (AttributeError, TypeError):
                                try:
                                    i_cost_bad = soup.find('span',
                                                           class_='a-size-base a-color-price offer-price a-text-normal').text.strip()
                                except (AttributeError, TypeError):
                                    try:
                                        i_cost_bad = soup.find('span',
                                                               class_='a-size-medium a-color-price').text.strip()
                                    except (AttributeError, TypeError):
                                        i_cost_bad = "$0.01"

        try:
            i_currency_good = self.get_currency(i_cost_bad)[0]
            i_cost_good = self.get_currency(i_cost_bad)[1]
        except (AttributeError, TypeError):
            pass

        try:
            i_photo_bad = soup.find('div', id='altImages').find_all('img')
            for a in i_photo_bad:
                b_photo = a['src'].replace('._SS40_','')
                if '.gif' in b_photo:
                    continue

                i_photo.append(b_photo)
        except (AttributeError, TypeError):
            try:
                i_photo_bad = soup.find('div', class_='a-row a-spacing-mini a-spacing-top-micro').find_all('img')
                for a in i_photo_bad:
                    p_temp = a['src'].replace('._AC_SY60_CR,0,0,60,60_', '')
                    i_photo.append(p_temp)
            except (AttributeError, TypeError):
                pass

        try:
            i_color_bad = soup.find('div', id='variation_color_name').find_all('img')
            for a in i_color_bad:
                i_color.append(a['alt'])
        except (AttributeError, TypeError):
            pass

        try:
            i_size_bad = soup.find('select', id='native_dropdown_selected_size_name').find_all('option')
            for a in i_size_bad:
                ab = re.sub(r'\s+', ' ', a.text).strip()
                if 'elect' in ab: continue
                i_size.append(ab)
        except (AttributeError, TypeError):
            pass

        end_json = self.go_to_json(i_enum, i_title, i_sku, i_shop_name, i_cost_good, i_currency_good,
                                   i_photo, i_color,i_size, self.url, i_country)
        return end_json