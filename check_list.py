
from oauth2client.service_account import ServiceAccountCredentials
import gspread
from gspread.exceptions import GSpreadException
import requests
import datetime
import html
import logging
from ebaysdk.finding import Connection as Finding
from string import ascii_lowercase

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from multiprocessing.dummy import Pool as ThreadPool

start = datetime.datetime.now()

pool = ThreadPool(8)

scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
creds = ServiceAccountCredentials.from_json_keyfile_name('SearchFromShop.json', scope)
client = gspread.authorize(creds)


etsy_api = 'o7ruFRjez81lzycRyh9n0g0z'
ebay_api = Finding(appid="RoinDa-f445-4db2-a42e-b060816d5cf7", config_file=None)

date = str(datetime.date.today())

# Логирование
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=logging.INFO,
                    handlers=[
                        logging.FileHandler("results.log"),
                        logging.StreamHandler()]
                    )

def get_data():
    try:
        logging.info('Получаем список слов')
        # Достаем слова из таблиц и делаем словарь, все
        words = client.open('BlackList').sheet1.get_all_records()
        words = {a['BlackWord'].lower(): a['WhiteStop'].lower() for a in words}

        # Превращаем в список стоп-слова, пустые строки не трогаем, чтобы не получилось [''] (True при if [''])
        for k in words:
            if words[k]:
                words[k] = words[k].split(', ')
    except GSpreadException as e:
        words = None
        logging.exception(e)

    try:
        logging.info('Получаем список лотов')
        # Список лотов (обязательно в первом столбце)
        lots = client.open('BlackList').get_worksheet(2).col_values(1)[1:] # Без названия столбца
    except GSpreadException as e:
        lots = None
        logging.info('\nERROR: Не удалось получить список лотов')

    try:
        logging.info('Получаем списки логинов с Etsy и Ebay')
        headers = {'ACCESS-KEY': 'ohgv0lQWhlf;wep094tjg0015t549549y5045y'}
        url = 'http://api.secretssite.com/maintence/logins_for_scan_blacklist'
        r = requests.get(url, headers=headers)
        logins = r.json()['logins']
        etsy_logins = [i for i in logins.keys() if logins[i] == 'etsy']
        ebay_logins = [i for i in logins.keys() if logins[i] == 'ebay']
        # etsy_logins = client.open('bbbb').get_worksheet(0).col_values(2)[1:]
        # ebay_logins = client.open('bbbb').get_worksheet(1).col_values(2)[1:]
    except GSpreadException as e:
        etsy_logins = ebay_logins = None
        log = e.__class__.__name__
        logging.info(log)

    return words, lots, etsy_logins, ebay_logins


def get_etsy(login):
    limit = 100      # лотов за запрос
    page = 1         # начиная с 1-й страницы
    reports = []
    log = 'Etsy ' + login + ' '
    try:
        while True:
            r = requests.get('https://openapi.etsy.com/v2/private/shops/' + login + '/listings/active',
                             params={
                                 'api_key': etsy_api,
                                 'limit': limit,
                                 'page': page
                             })

            if r.status_code == 200:
                posts = r.json().get('results')
                post_count = r.json().get('count')    # int
                reports_on_page = check_for_pass(posts, 'Etsy', login)
                if reports_on_page:
                    reports += reports_on_page
                if limit * page >= post_count:
                    break
                page += 1
            else:
                log += 'Error ' + str(r.status_code) + ' ' + r.content.decode('utf-8')
                return
        log += 'Success'
    except requests.exceptions.RequestException as e:
        log += 'Error ' + e.__class__.__name__
    finally:
        logging.info(log)
        return reports if reports else None



def get_ebay(login):
    limit = 100
    page = 1
    reports = []
    log = 'Ebay ' + login + ' '
    try:
        while True:
            request_parameters = {
                        'itemFilter': [
                            {'name': 'Seller', 'value': login},
                            {'name': 'HideDuplicateItems', 'value': True}
                        ],
                        'paginationInput': {
                            'entriesPer': limit,
                            'pageNumber': page
                        },
                        'sortOrder': 'StartTimeNewest'
                    }
            products = ebay_api.execute('findItemsAdvanced', request_parameters)
            response = products.dict()
            if response and response.get('searchResult'):
                posts = response.get('searchResult').get('item')
                reports_on_page = check_for_pass(posts, 'eBay', login)
                if reports_on_page:
                    reports += reports_on_page
            total_pages = int(response.get('paginationOutput').get('totalPages'))
            if page == total_pages:
                break
            page += 1
        log += 'Success'
    except ConnectionError as e:
        log += 'Error ' + e.__class__.__name__
    except Exception as e:     # На всякий случай, чтобы скрипт не остановился
        log += 'Error ' + e.__class__.__name__
    finally:
        logging.info(log)
    return reports if reports else None


def check_for_pass(posts, market, login):
    '''
    Принимает все лоты на одной странице, итерируется по лотам
    и проверяет названия.
    '''
    # global reports
    reports = []
    if market == 'eBay':
        api_tag = 'itemId'   # Для получения id лота в eBay и Etsy разные теги
    elif market == 'Etsy':
        api_tag = 'listing_id'
    if posts:
        for post in posts:
            exact_title = html.unescape(post['title'])    # Убираем непонятные символы
            title = exact_title.lower()
            title = ''.join([' ' if i not in ascii_lowercase else i for i in title])   # заменяем все не-буквы на пробелы
            title = title.split()          # Делаем список из слов
            post_id = post[api_tag]
            if int(post_id) not in lots:   # Проверяем на белые лоты
                for word in words:
                    # если слово есть в названии
                    if word in title:
                        # если нет стоп-слов или ни одного из стоп-слов нет в названии:
                        if not words[word] or all([i not in title for i in words[word]]):
                            report = market + " [" + login + "] Запрещенное слово (" + word + ") В заголовке - " + exact_title
                            with open('reports.txt', 'a+') as text_file:
                                text_file.write('\n' + report)
                            reports.append(report)
    return reports if reports else None


def send_mail(reports):
    logging.info('Отсылаем результаты на почту')
    if reports:
        text = '\n'.join(reports)
    else:
        text = 'Поиск лотов по blacklist завершен, ничего не найдено'
    mail_from = 'ticket@site.com'
    mail_to = 'so-support@site.com'
    mail_to = 'site@icloud.com'
    msg = MIMEMultipart()
    msg['From'] = mail_from
    msg['To'] = mail_to
    msg['Subject'] = '[Список] Запрещенные слова'
    body = text
    msg.attach(MIMEText(body, 'plain'))

    server = smtplib.SMTP('smtp.yandex.ru', 587)
    server.starttls()
    server.login(mail_from, 'Vfdvjee!3df5113')
    text = msg.as_string()

    server.sendmail(mail_from, mail_to, text)
    logging.info('Отправлено\n')
    server.quit()


def without_already_cheked(ebay_logins, etsy_logins):
    with open('results.log', 'r') as logs:
        logs_list = logs.read().split('\n')
        checked_logins = [log for log in logs_list if 'Success' in log]
        checked_ebay_logins = [login.split()[6] for login in checked_logins if login.split()[5] == 'Ebay']
        checked_etsy_logins = [login.split()[6] for login in checked_logins if login.split()[5] == 'Etsy']
        ebay_logins = [i for i in ebay_logins if i not in checked_ebay_logins]
        etsy_logins = [i for i in etsy_logins if i not in checked_etsy_logins]
        return  ebay_logins, etsy_logins



if __name__ == '__main__':
    words, lots, etsy_logins, ebay_logins = get_data()
    if not all([words, lots, etsy_logins, ebay_logins]):
        logging.info('Произошла ошибка при получении данных с Google Drive. Программа остановлена')
        exit()
    ebay_logins, etsy_logins = without_already_cheked(ebay_logins, etsy_logins)
    ebay_reports = pool.map(get_ebay, ebay_logins)
    etsy_reports = pool.map(get_etsy, etsy_logins)
    reports = etsy_reports  # ebay_reports
    reports = [report for report in reports if report]  # Чистим от None
    reports = [report for user in reports for report in user]  # лист листов --> лист строк
    try:
        send_mail(reports)
    except KeyboardInterrupt:
        raise
    except Exception as e:
        logging.info('Не отправлено! ' + e.__class__.__name__ + '\n')
    total_time = datetime.datetime.now() - start
    log = f'Завершено. Затраченное время {str(total_time)}'
    logging.info(log)


